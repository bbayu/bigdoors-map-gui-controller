package io.gitlab.bbayu.bigdoorscontroller;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;

public class CommandTest
{
    ServerMock server;
    BigDoorsControllerMain plugin;
    PlayerMock player;
    
    @Before
    public void setUp()
    {
        server = MockBukkit.mock();
        plugin = MockBukkit.load(BigDoorsControllerMain.class);
        player = server.addPlayer();
    }
    
    @After
    public void tearDown()
    {
        MockBukkit.unload();
    }
    
    @Test
    public void testCommandNoPermission()
    {
        player.performCommand("bigdoorsguicontroller");
        String message = player.nextMessage();
        System.out.println(message);
        assertTrue("Checking if the \"No Permission\" or similar message is sent",
                message.toLowerCase().contains("do not have permission")
                        || message.toLowerCase().contains("no permission"));
    }
    
    @Test
    public void testCommandNoSubCmd()
    {
        player.addAttachment(plugin).setPermission("bigdoors.gui.controller.create", true);
        player.performCommand("bigdoorsguicontroller");
        String message = player.nextMessage();
        System.out.println(message);
        assertTrue("Checking if a Usage message is sent", message.toLowerCase().contains("usage"));
    }
    
    @Test
    public void testCommandGetNoArg()
    {
        player.addAttachment(plugin).setPermission("bigdoors.gui.controller.create", true);
        player.performCommand("bigdoorsguicontroller get");
        String message = player.nextMessage();
        System.out.println(message);
        assertTrue("Checking if a Usage message is sent", message.toLowerCase().contains("usage"));
    }
}
