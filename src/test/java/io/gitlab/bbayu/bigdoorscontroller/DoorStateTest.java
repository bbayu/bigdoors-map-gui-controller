package io.gitlab.bbayu.bigdoorscontroller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mockito.Mockito;

import nl.pim16aap2.bigDoors.Door;

public class DoorStateTest
{
    @Test
    public void testSingleDoorState()
    {
        
        Door door1 = Mockito.mock(Door.class);
        Door door2 = Mockito.mock(Door.class);
        Mockito.when(door2.isOpen()).thenReturn(true);
        
        assertEquals(DoorState.CLOSED, DoorState.fromDoor(door1, false));
        assertEquals(DoorState.OPENED, DoorState.fromDoor(door2, false));
        assertEquals(DoorState.OPENING, DoorState.fromDoor(door1, true));
        assertEquals(DoorState.CLOSING, DoorState.fromDoor(door2, true));
    }
}
