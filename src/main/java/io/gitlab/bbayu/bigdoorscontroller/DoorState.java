package io.gitlab.bbayu.bigdoorscontroller;

import nl.pim16aap2.bigDoors.BigDoors;
import nl.pim16aap2.bigDoors.Door;

public enum DoorState
{
    OPENING,
    OPENED,
    CLOSING,
    CLOSED;
    
    public static DoorState fromDoor(Door door)
    {
        return fromDoor(door, BigDoors.get().getCommander().isDoorBusy(door.getDoorUID()));
    }
    
    public static DoorState fromDoor(Door door, boolean doorIsBusy)
    {
        if (door.isOpen() && doorIsBusy) return DoorState.CLOSING;
        else if (!door.isOpen() && doorIsBusy) return DoorState.OPENING;
        else if (door.isOpen() && !doorIsBusy) return DoorState.OPENED;
        else return DoorState.CLOSED;
    }
}
