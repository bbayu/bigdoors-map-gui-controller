package io.gitlab.bbayu.bigdoorscontroller;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import com.bergerkiller.bukkit.common.map.MapDisplay;
import com.bergerkiller.bukkit.common.utils.ItemUtil;

import io.gitlab.bbayu.bigdoorscontroller.controller.DoorController;
import nl.pim16aap2.bigDoors.BigDoors;
import nl.pim16aap2.bigDoors.events.DoorEventToggleEnd;
import nl.pim16aap2.bigDoors.events.DoorEventToggleStart;

public class BigDoorsControllerMain extends JavaPlugin implements Listener
{
    boolean isTestingEnvironment = false;
    
    public BigDoorsControllerMain()
    {
        super();
        isTestingEnvironment = false;
    }
    
    protected BigDoorsControllerMain(JavaPluginLoader loader, PluginDescriptionFile description,
            File dataFolder, File file)
    {
        super(loader, description, dataFolder, file);
        isTestingEnvironment = true;
    }
    
    @Override
    public void onEnable()
    {
        final PluginManager pm = getServer().getPluginManager();
        if (!isTestingEnvironment && pm.getPlugin("BKCommonLib") == null)
        {
            getLogger().severe("UNABLE TO FIND BKCOMMONLIB! THIS PLUGIN REQUIRES BKCOMMONLIB. "
                    + "UNTIL IT IS INSTALLED, THIS PLUGIN WILL REMAIN DISABLED!");
            pm.disablePlugin(this);
            return;
        }
        if (!isTestingEnvironment && pm.getPlugin("BigDoors") == null)
        {
            getLogger().severe("UNABLE TO FIND BIGDOORS! THIS PLUGIN REQUIRES BIGDOORS. "
                    + "UNTIL IT IS INSTALLED, THIS PLUGIN WILL REMAIN DISABLED!");
            pm.disablePlugin(this);
            return;
        }
        registerCommandsEvents();
    }
    
    @Override
    public void onDisable()
    {
    }
    
    private void registerCommandsEvents()
    {
        registerCommandEvent(this, "bigdoorsguicontroller");
    }
    
    private <T extends CommandExecutor> void registerCommand(final T clazz, final String cmd)
    {
        getCommand(cmd).setExecutor(clazz);
    }
    
    private <T extends Listener> void registerEvent(final T clazz)
    {
        getServer().getPluginManager().registerEvents(clazz, this);
    }
    
    private <T extends CommandExecutor & Listener> void registerCommandEvent(final T clazz, final String cmd)
    {
        registerCommand(clazz, cmd);
        registerEvent(clazz);
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
    {
        if (!(sender instanceof Player))
        {
            sender.sendMessage("You must be a player in order to do this!");
            return true;
        }
        
        Player player = (Player) sender;
        
        if (args.length == 0) return false;
        else if (!player.hasPermission("bigdoors.gui.controller.create"))
        {
            player.sendMessage(ChatColor.RED + "You do not have permission to do this!");
            return true;
        }
        else if (args[0].equalsIgnoreCase("get"))
        {
            if (args.length == 1)
                player.sendMessage(ChatColor.GOLD + "Usage: /" + cmd.getName() + " " + args[0] + " <name>");
            else
            {
                ItemStack item = MapDisplay.createMapItem(DoorController.class);
                ItemUtil.setDisplayName(item, "BigDoors Map GUI Controller");
                ItemUtil.getMetaTag(item).putValue("name", args[1]);
                ItemUtil.getMetaTag(item).putValue("owner", player.getUniqueId());
                ItemUtil.getMetaTag(item).putValue("public", false);
                player.getInventory().addItem(item);
                player.sendMessage(ChatColor.GREEN + "Obtained New BigDoors Map GUI Controller");
            }
            
        }
        else if (MapDisplay.getHeldDisplay(player, DoorController.class) == null)
        {
            player.sendMessage(
                    ChatColor.RED + "You must hold a BigDoors Map GUI Controller in order to do this!");
            return true;
        }
        else
        {
            ItemStack item = player.getInventory().getItemInMainHand();
            UUID uuid = ItemUtil.getMetaTag(item).getValue("owner", UUID.class);
            
            if (!player.getUniqueId().equals(uuid)
                    && !player.hasPermission("bigdoors.gui.controller.admincreate"))
            {
                player.sendMessage(ChatColor.RED + "You do not own this GUI Controller! "
                        + "Only the Controller owner or OP can modify this Controller!");
                return true;
            }
            
            DoorController controller = MapDisplay.getHeldDisplay(player, DoorController.class);
            if (args[0].equalsIgnoreCase("adddoor"))
            {
                if (args.length == 1) player.sendMessage(
                        ChatColor.GOLD + "Usage: /" + cmd.getName() + " " + args[0] + " <doorID>");
                else try
                {
                    if (BigDoors.get().getCommander().getDoor(null, Long.parseLong(args[1])) == null)
                        player.sendMessage(ChatColor.RED + "A door with that ID does not exist!");
                    else if (BigDoors.get().getCommander().getDoor(uuid, Long.parseLong(args[1])) == null)
                        player.sendMessage(ChatColor.RED + "You don't own that door! Cannot add to GUI!");
                    else
                    {
                        if (controller.addDoor(Long.parseLong(args[1]))) player.sendMessage(
                                ChatColor.GREEN + "Added doorID " + args[1] + " to this controller!");
                        else player.sendMessage(
                                ChatColor.RED + "DoorID " + args[1] + " is already on this controller!");
                        controller.reload();
                    }
                }
                catch (NumberFormatException e)
                {
                    player.sendMessage(ChatColor.RED + "DoorID needs to be a number!");
                }
            }
            else if (args[0].equalsIgnoreCase("remdoor"))
            {
                if (args.length == 1) player.sendMessage(
                        ChatColor.GOLD + "Usage: /" + cmd.getName() + " " + args[0] + " <doorID>");
                else try
                {
                    if (controller.removeDoor(Long.parseLong(args[1]))) player.sendMessage(
                            ChatColor.GREEN + "Removed doorID " + args[1] + " from this controller!");
                    else player
                            .sendMessage(ChatColor.RED + "DoorID " + args[1] + " is not on this controller!");
                    controller.reload();
                }
                catch (NumberFormatException e)
                {
                    player.sendMessage(ChatColor.RED + "DoorID needs to be a number!");
                }
            }
            else if (args[0].equalsIgnoreCase("listdoors"))
            {
                player.sendMessage(ChatColor.GREEN + "Doors on this controller:");
                for (String msg : controller.listDoors())
                {
                    player.sendMessage("- " + ChatColor.GRAY + msg);
                }
            }
            else if (args[0].equalsIgnoreCase("alias"))
            {
                if (args.length == 1) player.sendMessage(
                        ChatColor.GOLD + "Usage: /" + cmd.getName() + " " + args[0] + " <doorID> [name]");
                else try
                {
                    long doorID = Long.parseLong(args[1]);
                    if (!controller.hasDoor(doorID))
                    {
                        player.sendMessage(
                                ChatColor.RED + "DoorID " + args[1] + " is not on this controller!");
                        return true;
                    }
                    else if (args.length == 2)
                    {
                        controller.setAlias(doorID, null);
                        player.sendMessage(ChatColor.GREEN + "Removed alias from doorID " + args[1] + "!");
                    }
                    else if (args[2].length() > 8)
                    {
                        player.sendMessage(
                                ChatColor.RED + "The alias cannot be longer than 8 characters long!");
                        return true;
                    }
                    else if (!StandardCharsets.US_ASCII.newEncoder().canEncode(args[2]))
                    {
                        player.sendMessage(
                                ChatColor.RED + "The alias must be ASCII compatible, no fancy letters!");
                        return true;
                    }
                    else
                    {
                        controller.setAlias(doorID, args[2]);
                        player.sendMessage(
                                ChatColor.GREEN + "Set alias of doorID " + args[1] + " to " + args[2] + "!");
                    }
                    controller.reload();
                }
                catch (NumberFormatException e)
                {
                    player.sendMessage(ChatColor.RED + "DoorID needs to be a number!");
                }
            }
            else if (args[0].equalsIgnoreCase("public") || args[0].equalsIgnoreCase("unlock"))
            {
                controller.setPublic(true);
                controller.reload();
                
                player.sendMessage(ChatColor.GREEN + "Everyone can now open doors on this controller!");
                
            }
            else if (args[0].equalsIgnoreCase("private") || args[0].equalsIgnoreCase("lock"))
            {
                controller.setPublic(false);
                controller.reload();
                
                player.sendMessage(
                        ChatColor.GREEN + "Only the owner and OPs can open doors on this controller!");
            }
            else player.sendMessage(ChatColor.RED + "Unknown argument!");
        }
        return true;
    }
    
    private void updateAllMaps(long doorID)
    {
        Collection<DoorController> controllers = MapDisplay.getAllDisplays(DoorController.class);
        for (DoorController controller : controllers)
            controller.sendStatusChange("doorStatusUpdate", doorID);
    }
    
    @EventHandler
    public void onDoorToggleStart(DoorEventToggleStart event)
    {
        updateAllMaps(event.getDoor().getDoorUID());
    }
    
    @EventHandler
    public void onDoorToggleEnd(DoorEventToggleEnd event)
    {
        updateAllMaps(event.getDoor().getDoorUID());
    }
    
    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e)
    {
        if (e.getEntity() instanceof ItemFrame && ((ItemFrame) e.getEntity()).getItem() != null)
            ((ItemFrame) e.getEntity()).setCustomNameVisible(true);
    }
    
    @EventHandler
    public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent e)
    {
        if (e.getRightClicked() instanceof ItemFrame && MapDisplay.getHeldDisplay(e.getPlayer()) != null)
            ((ItemFrame) e.getRightClicked()).setCustomNameVisible(false);
    }
}
