package io.gitlab.bbayu.bigdoorscontroller.widgets;

import java.awt.Color;

import com.bergerkiller.bukkit.common.map.MapColorPalette;
import com.bergerkiller.bukkit.common.map.MapFont;
import com.bergerkiller.bukkit.common.map.widgets.MapWidgetText;

public class MapWidgetTextColorable extends MapWidgetText
{
    private String _text = "";
    private Color _textColor = new Color(224, 224, 224), _textShadow = new Color(56, 56, 56);
    private boolean _smallFont = false;
    
    public MapWidgetTextColorable()
    {
        setFocusable(false);
    }
    
    /**
     * Sets the text displayed
     * 
     * @param text
     *            to display, null or empty to show no text
     * @return this text widget
     */
    @Override
    public MapWidgetTextColorable setText(String text)
    {
        if (!_text.equals(text))
        {
            _text = text;
            if (_text == null)
            {
                _text = "";
            }
            invalidate();
        }
        return this;
    }
    
    /**
     * Gets the text currently displayed
     * 
     * @return displayed text
     */
    @Override
    public String getText()
    {
        return _text;
    }
    
    public MapWidgetTextColorable setSmallFont(boolean smallFont)
    {
        _smallFont = smallFont;
        invalidate();
        return this;
    }
    
    public boolean isSmallFont()
    {
        return _smallFont;
    }
    
    /**
     * Sets the text color
     * 
     * @param textColor
     *            Color to display, null to show default color
     * @return this text widget
     */
    public MapWidgetTextColorable setTextColor(Color textColor)
    {
        if (!_textColor.equals(textColor))
        {
            _textColor = textColor;
            if (_textColor == null)
            {
                _textColor = new Color(224, 224, 224);
            }
            invalidate();
        }
        return this;
    }
    
    /**
     * Gets the text color
     * 
     * @return text color
     */
    public Color getTextColor()
    {
        return _textColor;
    }
    
    /**
     * Sets the text shadow color
     * 
     * @param textShadow
     *            Color to display, null to show default color
     * @return this text widget
     */
    public MapWidgetTextColorable setTextShadow(Color textShadow)
    {
        if (!_textShadow.equals(textShadow))
        {
            _textShadow = textShadow;
            if (_textShadow == null)
            {
                _textShadow = new Color(56, 56, 56);
            }
            invalidate();
        }
        return this;
    }
    
    /**
     * Gets the text shadow color
     * 
     * @return text shadow color
     */
    public Color getTextShadow()
    {
        return _textShadow;
    }
    
    @Override
    public void onDraw()
    {
        byte textColor, textShadowColor;
        if (!isEnabled())
        {
            textColor = MapColorPalette.getColor(160, 160, 160);
            textShadowColor = MapColorPalette.COLOR_TRANSPARENT;
        }
        else
        {
            textColor = MapColorPalette.getColor(_textColor);
            textShadowColor = MapColorPalette.getColor(_textShadow);
        }
        
        if (!_text.isEmpty())
        {
            MapFont<Character> font = _smallFont ? MapFont.TINY : MapFont.MINECRAFT;
            view.setAlignment(MapFont.Alignment.LEFT);
            if (textShadowColor != MapColorPalette.COLOR_TRANSPARENT)
            {
                view.draw(font, 1, 1, textShadowColor, _text);
            }
            view.draw(font, 0, 0, textColor, _text);
        }
    }
}
