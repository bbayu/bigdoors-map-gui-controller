package io.gitlab.bbayu.bigdoorscontroller.widgets;

import java.awt.Color;
import java.awt.Dimension;

import com.bergerkiller.bukkit.common.events.map.MapStatusEvent;
import com.bergerkiller.bukkit.common.map.MapCanvas;
import com.bergerkiller.bukkit.common.map.MapColorPalette;
import com.bergerkiller.bukkit.common.map.MapFont;
import com.bergerkiller.bukkit.common.map.widgets.MapWidgetButton;

public class MapWidgetButtonColorable extends MapWidgetButton
{
    private String _text = "";
    private Color _textColor = new Color(224, 224, 224), _textShadow = new Color(56, 56, 56),
            _textFocusColor = new Color(255, 255, 160), _textFocusShadow = new Color(63, 63, 40);
    private Color _backgroundTopColor = new Color(170, 170, 170), _backgroundColor = new Color(111, 111, 111),
            _backgroundBottomColor = new Color(86, 86, 86),
            _backgroundFocusTopColor = new Color(190, 200, 255),
            _backgroundFocusColor = new Color(126, 136, 191),
            _backgroundFocusBottomColor = new Color(92, 102, 157);
    private boolean _smallFont = false;
    
    public MapWidgetButtonColorable()
    {
        setFocusable(true);
    }
    
    /**
     * Sets the text displayed in this button
     * 
     * @param text
     *            to display, null or empty to show no text
     * @return this menu button widget
     */
    @Override
    public MapWidgetButtonColorable setText(String text)
    {
        if (!_text.equals(text))
        {
            _text = text;
            if (_text == null)
            {
                _text = "";
            }
            invalidate();
        }
        return this;
    }
    
    /**
     * Gets the text currently displayed in this button
     * 
     * @return displayed text
     */
    @Override
    public String getText()
    {
        return _text;
    }
    
    public MapWidgetButtonColorable setSmallFont(boolean smallFont)
    {
        _smallFont = smallFont;
        invalidate();
        return this;
    }
    
    public boolean isSmallFont()
    {
        return _smallFont;
    }
    
    /**
     * Sets the text color for this button
     * 
     * @param backgroundColor
     *            Color to display, null to show default color
     * @return this menu button widget
     */
    public MapWidgetButtonColorable setBackgroundColor(Color backgroundColor)
    {
        if (!_backgroundColor.equals(backgroundColor))
        {
            _backgroundColor = backgroundColor;
            if (_backgroundColor == null)
            {
                _backgroundColor = new Color(111, 111, 111);
                _backgroundTopColor = new Color(170, 170, 170);
                _backgroundBottomColor = new Color(86, 86, 86);
                _textColor = new Color(224, 224, 224);
                _textShadow = new Color(56, 56, 56);
            }
            
            else
            {
                _backgroundTopColor = MapColorPalette.getRealColor(
                        MapColorPalette.getSpecular(MapColorPalette.getColor(backgroundColor), 1.3f));
                _backgroundBottomColor = MapColorPalette.getRealColor(
                        MapColorPalette.getSpecular(MapColorPalette.getColor(backgroundColor), 0.7f));
                _textColor = getContrastColor(backgroundColor, new Color(0xAAAAAA), new Color(0x555555));
                _textShadow = MapColorPalette.getRealColor(
                        MapColorPalette.getSpecular(MapColorPalette.getColor(_textColor), 0.7f));
            }
            invalidate();
        }
        return this;
    }
    
    /**
     * Sets the background focus color for this button
     * 
     * @param backgroundFocusColor
     *            Color to display, null to show default color
     * @return this menu button widget
     */
    public MapWidgetButtonColorable setBackgroundFocusColor(Color backgroundFocusColor)
    {
        if (!_backgroundFocusColor.equals(backgroundFocusColor))
        {
            _backgroundFocusColor = backgroundFocusColor;
            if (_backgroundFocusColor == null)
            {
                _backgroundFocusColor = new Color(190, 200, 255);
                _backgroundFocusTopColor = new Color(126, 136, 191);
                _backgroundFocusBottomColor = new Color(92, 102, 157);
                _textFocusColor = new Color(255, 255, 160);
                _textFocusShadow = new Color(63, 63, 40);
            }
            else
            {
                _backgroundFocusTopColor = MapColorPalette.getRealColor(
                        MapColorPalette.getSpecular(MapColorPalette.getColor(backgroundFocusColor), 1.3f));
                _backgroundFocusBottomColor = MapColorPalette.getRealColor(
                        MapColorPalette.getSpecular(MapColorPalette.getColor(backgroundFocusColor), 0.7f));
                _textFocusColor = getContrastColor(backgroundFocusColor, new Color(0xFFFFFF),
                        new Color(0x000000));
                _textFocusShadow = MapColorPalette.getRealColor(
                        MapColorPalette.getSpecular(MapColorPalette.getColor(_textColor), 0.7f));
            }
            invalidate();
        }
        return this;
    }
    
    /**
     * Gets the background focus color for this button
     * 
     * @return background focus color
     */
    public Color getBackgroundFocusColor()
    {
        return _backgroundFocusColor;
    }
    
    @Override
    public void onDraw()
    {
        byte textColor, textShadowColor;
        if (!isEnabled())
        {
            textColor = MapColorPalette.getColor(160, 160, 160);
            textShadowColor = MapColorPalette.COLOR_TRANSPARENT;
        }
        else if (isFocused())
        {
            textColor = MapColorPalette.getColor(_textFocusColor);
            textShadowColor = MapColorPalette.getColor(_textFocusShadow);
        }
        else
        {
            textColor = MapColorPalette.getColor(_textColor);
            textShadowColor = MapColorPalette.getColor(_textShadow);
        }
        
        fillButtonBackground(view, isEnabled(), isFocused());
        
        // Draw the text inside the button
        if (!_text.isEmpty())
        {
            MapFont<Character> font = _smallFont ? MapFont.TINY : MapFont.MINECRAFT;
            Dimension textSize = view.calcFontSize(font, _text);
            int textX = (getWidth() - textSize.width) / 2 + 1;
            int textY = (getHeight() - textSize.height) / 2;
            
            view.setAlignment(MapFont.Alignment.LEFT);
            if (textShadowColor != MapColorPalette.COLOR_TRANSPARENT)
                view.draw(font, textX + 1, textY + 1, textShadowColor, _text);
            view.draw(font, textX, textY, textColor, _text);
        }
    }
    
    /**
     * Draws the background texture for a button onto a canvas, filling it entirely.
     * For filling only a portion, use a view.
     * 
     * @param canvas
     *            to draw on
     * @param enabled
     *            whether button is enabled
     * @param focused
     *            whether button is focused
     */
    public void fillButtonBackground(MapCanvas canvas, boolean enabled, boolean focused)
    {
        byte topEdgeColor, fillColor, btmEdgeColor;
        if (!enabled)
        {
            topEdgeColor = fillColor = btmEdgeColor = MapColorPalette.getColor(44, 44, 44);
        }
        else if (!focused)
        {
            topEdgeColor = MapColorPalette.getColor(_backgroundTopColor);
            fillColor = MapColorPalette.getColor(_backgroundColor);
            btmEdgeColor = MapColorPalette.getColor(_backgroundBottomColor);
        }
        else
        {
            topEdgeColor = MapColorPalette.getColor(_backgroundFocusTopColor);
            fillColor = MapColorPalette.getColor(_backgroundFocusColor);
            btmEdgeColor = MapColorPalette.getColor(_backgroundFocusBottomColor);
        }
        
        int x1 = 0, y1 = 0;
        int x2 = canvas.getWidth() - 1, y2 = canvas.getHeight() - 1;
        
        canvas.drawLine(x1, y1, x2 - 1, y1, topEdgeColor);
        canvas.drawLine(x1, y1 + 1, x1, y2 - 2, topEdgeColor);
        canvas.drawLine(x1, y2 - 1, x1, y2, fillColor);
        canvas.drawPixel(x2, y1, fillColor);
        canvas.drawLine(x1 + 1, y2 - 1, x2, y2 - 1, btmEdgeColor);
        canvas.drawLine(x1 + 1, y2, x2, y2, btmEdgeColor);
        canvas.drawLine(x2, 1, x2, y2 - 2, btmEdgeColor);
        canvas.fillRectangle(x1 + 1, y1 + 1, x2 - x1 - 1, y2 - x1 - 2, fillColor);
    }
    
    @Override
    public void onStatusChanged(MapStatusEvent event)
    {
        super.onStatusChanged(event);
        if (event.isName("buttonClick"))
        {
            String regex = "(\\d+),(\\d+)";
            String argument = event.getArgument(String.class);
            if (argument != null && argument.matches(regex))
            {
                int x = Integer.parseInt(argument.replaceAll(regex, "$1"));
                int y = Integer.parseInt(argument.replaceAll(regex, "$2"));
                if (x >= getAbsoluteX() && x < getAbsoluteX() + getWidth() && y >= getAbsoluteY()
                        && y < getAbsoluteY() + getHeight())
                {
                    focus();
                    activate();
                }
            }
        }
    }
    
    public Color getContrastColor(Color bgColor, Color lightColor, Color darkColor)
    {
        int r = bgColor.getRed();
        int g = bgColor.getGreen();
        int b = bgColor.getBlue();
        return (((r * 0.299) + (g * 0.587) + (b * 0.114)) > 186) ? darkColor : lightColor;
    }
}
