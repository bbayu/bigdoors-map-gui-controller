package io.gitlab.bbayu.bigdoorscontroller.widgets;

import java.awt.Color;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import com.bergerkiller.bukkit.common.events.map.MapStatusEvent;
import com.bergerkiller.bukkit.common.map.MapColorPalette;
import com.bergerkiller.bukkit.common.map.widgets.MapWidget;
import com.bergerkiller.bukkit.common.resources.SoundEffect;

import io.gitlab.bbayu.bigdoorscontroller.DoorState;
import nl.pim16aap2.bigDoors.BigDoors;
import nl.pim16aap2.bigDoors.Commander;
import nl.pim16aap2.bigDoors.Door;

public class MapWidgetListItem extends MapWidget implements Listener
{
    private final Commander commander = BigDoors.get().getCommander();
    
    private final long doorID;
    
    private final int order;
    private boolean stateChanged;
    
    private final MapWidgetTextColorable displayWidget;
    private final MapWidgetDoorState stateWidget;
    private final MapWidgetButtonColorable openButton, closeButton;
    
    public MapWidgetListItem(long doorID, int order, String name)
    {
        this.order = order;
        this.doorID = doorID;
        
        onDoorTrigger();
        
        displayWidget = new MapWidgetTextColorable()
        {
            @Override
            public void onAttached()
            {
                setClipParent(true);
                setText(name);
                setSmallFont(name.length() > 5);
            }
        };
        
        stateWidget = new MapWidgetDoorState()
        {
            @Override
            public void onAttached()
            {
                setClipParent(true);
                setState(DoorState.fromDoor(commander.getDoor(null, doorID), commander.isDoorBusy(doorID)));
                onDoorTrigger();
            }
            
            @Override
            public void onStatusChanged(MapStatusEvent event)
            {
                super.onStatusChanged(event);
                if (event.isName("doorStatusUpdate") || event.isName("doorOpen") || event.isName("doorClose"))
                    if (event.getArgument(Long.class).equals(doorID)) onDoorTrigger();
            }
            
            @Override
            public void onTick()
            {
                if (stateChanged)
                {
                    stateChanged = false;
                    setState(DoorState.fromDoor(commander.getDoor(null, doorID),
                            commander.isDoorBusy(doorID)));
                    invalidate();
                }
            }
        };
        
        openButton = new MapWidgetButtonColorable()
        {
            @Override
            public void onAttached()
            {
                setClipParent(true);
                setText("Open");
                setSmallFont(true);
                setBackgroundFocusColor(new Color(0x55FF55));
            }
            
            @Override
            public void onFocus()
            {
                display.playSound(SoundEffect.CLICK_WOOD);
                sendStatusChange("doorBtnFocusOpen", order);
            }
            
            @Override
            public void onActivate()
            {
                display.playSound(SoundEffect.fromName("block.note_block.hearp"), 1.0f, 1.0f);
                sendStatusChange("doorOpen", doorID);
            }
        };
        
        closeButton = new MapWidgetButtonColorable()
        {
            @Override
            public void onAttached()
            {
                setClipParent(true);
                setText("Close");
                setSmallFont(true);
                setBackgroundFocusColor(new Color(0xFF5555));
            }
            
            @Override
            public void onFocus()
            {
                display.playSound(SoundEffect.CLICK_WOOD);
                sendStatusChange("doorBtnFocusClose", order);
            }
            
            @Override
            public void onActivate()
            {
                display.playSound(SoundEffect.fromName("block.note_block.pling"), 1.0f, 0.5f);
                sendStatusChange("doorClose", doorID);
            }
        };
    }
    
    @Override
    public void onAttached()
    {
        setClipParent(true);
        
        this.addWidget(displayWidget).setBounds(1, 3, 40, 10);
        this.addWidget(stateWidget).setBounds(33, 3, 42, 10);
        this.addWidget(openButton).setBounds(73, 1, 24, 12);
        this.addWidget(closeButton).setBounds(97, 1, 24, 12);
    }
    
    @Override
    public void onDraw()
    {
        if (order % 2 == 0)
            view.fillRectangle(0, 0, getWidth(), getHeight(), MapColorPalette.getColor(174, 174, 174));
        else view.fillRectangle(0, 0, getWidth(), getHeight(), MapColorPalette.getColor(148, 148, 148));
    }
    
    @Override
    public void onStatusChanged(MapStatusEvent event)
    {
        super.onStatusChanged(event);
        if (event.isName("doorOpen"))
        {
            if (event.getArgument(Long.class).equals(doorID))
            {
                triggerDoors(true);
            }
        }
        else if (event.isName("doorClose"))
        {
            if (event.getArgument(Long.class).equals(doorID))
            {
                triggerDoors(false);
            }
        }
    }
    
    public long getDoorID()
    {
        return doorID;
    }
    
    public void setAlias(String name)
    {
        if (name == null) name = "#" + doorID;
        displayWidget.setText(name);
        displayWidget.setSmallFont(name.length() > 5);
        Bukkit.broadcastMessage(name);
        Bukkit.broadcastMessage("#" + (name.length() > 5));
        displayWidget.invalidate();
    }
    
    public void triggerDoors(boolean open)
    {
        Door door = commander.getDoor(null, doorID);
        if (door.isOpen() != open) BigDoors.get().getDoorOpener(door.getType()).openDoor(door, 0.0);
    }
    
    public void onDoorTrigger()
    {
        stateChanged = true;
    }
    
    public MapWidgetTextColorable getDisplayWidget()
    {
        return displayWidget;
    }
    
    public MapWidgetDoorState getStateWidget()
    {
        return stateWidget;
    }
    
    public MapWidgetButtonColorable getOpenButton()
    {
        return openButton;
    }
    
    public MapWidgetButtonColorable getCloseButton()
    {
        return closeButton;
    }
}
