package io.gitlab.bbayu.bigdoorscontroller.widgets;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.bergerkiller.bukkit.common.events.map.MapStatusEvent;
import com.bergerkiller.bukkit.common.map.widgets.MapWidget;

public class MapWidgetListView extends MapWidget
{
    private final static int WIDGET_HEIGHT = 17;
    
    private final List<Long> doorIDs;
    private final Map<Long, String> doorAliases;
    private List<MapWidgetListItem> widgets;
    private int offset = 0;
    private int focus = -1;
    
    public MapWidgetListView(List<Long> doorIDs, Map<Long, String> doorAliases)
    {
        this.doorIDs = doorIDs;
        this.doorAliases = doorAliases;
    }
    
    @Override
    public void onAttached()
    {
        offset = 0;
        loadWidgets();
        drawWidgets();
    }
    
    private void loadWidgets()
    {
        widgets = new ArrayList<>();
        for (int i = 0; i < doorIDs.size(); i++)
        {
            long doorID = doorIDs.get(i);
            String name = doorAliases.getOrDefault(doorID, "#" + doorID);
            widgets.add(new MapWidgetListItem(doorID, i, name));
        }
    }
    
    private void drawWidgets()
    {
        clearWidgets();
        for (int i = 0; i < doorIDs.size(); i++)
            addWidget(widgets.get(i)).setBounds(0, i * WIDGET_HEIGHT + offset, getWidth(), WIDGET_HEIGHT);
    }
    
    private void moveWidgets()
    {
        for (int i = 0; i < doorIDs.size(); i++)
            widgets.get(i).setPosition(0, i * WIDGET_HEIGHT + offset);
    }
    
    @Override
    public void onStatusChanged(MapStatusEvent event)
    {
        super.onStatusChanged(event);
        if (event.isName("doorBtnFocusOpen") || event.isName("doorBtnFocusClose"))
        {
            int index = event.getArgument(Integer.class);
            if (focus != index)
            {
                int yPos = widgets.get(index).getY();
                boolean move = false;
                if (yPos < 0)
                {
                    offset -= yPos;
                    move = true;
                }
                else if (yPos > getHeight() - WIDGET_HEIGHT)
                {
                    offset += (getHeight() - WIDGET_HEIGHT) - yPos;
                    move = true;
                }
                if (move) moveWidgets();
                if (event.isName("doorBtnFocusOpen")) widgets.get(index).getOpenButton().focus();
                else widgets.get(index).getCloseButton().focus();
            }
        }
    }
    
    public List<Long> getDoorIDs()
    {
        return doorIDs;
    }
    
    public void setAlias(long doorID, String alias)
    {
        if (alias == null) doorAliases.remove(doorID);
        else doorAliases.put(doorID, alias);
        
        for (MapWidgetListItem widget : widgets)
            if (widget.getDoorID() == doorID)
            {
                widget.setAlias(alias);
                widget.invalidate();
                break;
            }
    }
}
