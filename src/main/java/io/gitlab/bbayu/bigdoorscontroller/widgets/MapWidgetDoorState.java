package io.gitlab.bbayu.bigdoorscontroller.widgets;

import java.util.HashMap;
import java.util.Map;

import com.bergerkiller.bukkit.common.map.MapColorPalette;
import com.bergerkiller.bukkit.common.map.MapFont;
import com.bergerkiller.bukkit.common.map.widgets.MapWidget;

import io.gitlab.bbayu.bigdoorscontroller.DoorState;

public class MapWidgetDoorState extends MapWidget
{
    private DoorState _state;
    private static final Map<DoorState, Byte> colors = new HashMap<>();
    
    static
    {
        colors.put(DoorState.CLOSED, MapColorPalette.COLOR_RED);
        colors.put(DoorState.OPENED, MapColorPalette.COLOR_GREEN);
        colors.put(DoorState.OPENING, MapColorPalette.COLOR_YELLOW);
        colors.put(DoorState.CLOSING, MapColorPalette.COLOR_ORANGE);
    }
    
    public MapWidgetDoorState()
    {
        setFocusable(false);
    }
    
    /**
     * Sets the state displayed
     * 
     * @param state
     *            to display
     * @return this menu state widget
     */
    public MapWidgetDoorState setState(DoorState state)
    {
        if (state != null && _state != state)
        {
            _state = state;
            invalidate();
        }
        return this;
    }
    
    /**
     * Gets the state currently displayed
     * 
     * @return displayed state
     */
    public DoorState getState()
    {
        return _state;
    }
    
    @Override
    public void onDraw()
    {
        if (isEnabled())
        {
            view.draw(MapFont.MINECRAFT, 1, 1, MapColorPalette.getSpecular(colors.get(_state), 0.7f),
                    _state.name());
            view.draw(MapFont.MINECRAFT, 0, 0, colors.get(_state), _state.name());
        }
        else
        {
            view.draw(MapFont.MINECRAFT, 0, 0, MapColorPalette.getColor(160, 160, 160), _state.name());
        }
    }
}
