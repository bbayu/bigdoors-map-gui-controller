package io.gitlab.bbayu.bigdoorscontroller.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.bergerkiller.bukkit.common.events.map.MapClickEvent;
import com.bergerkiller.bukkit.common.events.map.MapKeyEvent;
import com.bergerkiller.bukkit.common.map.MapDisplay;
import com.bergerkiller.bukkit.common.map.MapPlayerInput.Key;
import com.bergerkiller.bukkit.common.map.MapSessionMode;
import com.bergerkiller.bukkit.common.map.widgets.MapWidgetWindow;
import com.bergerkiller.bukkit.common.resources.SoundEffect;

import io.gitlab.bbayu.bigdoorscontroller.widgets.MapWidgetButtonColorable;
import io.gitlab.bbayu.bigdoorscontroller.widgets.MapWidgetListView;

public class DoorController extends MapDisplay
{
    private boolean sneakWalking = false;
    
    private MapWidgetWindow window = new MapWidgetWindow();
    
    private MapWidgetListView doorList;
    
    private String name;
    private UUID owner;
    
    private boolean _public = false;
    
    @Override
    public void onTick()
    {
        if (getViewers().size() == 0) return;
        Player player = getViewers().get(0);
        
        // Allow walking around when sneaking
        if (sneakWalking && !player.isSneaking())
        {
            sneakWalking = false;
            setReceiveInputWhenHolding(true);
        }
    }
    
    public boolean updateSneakWalking(MapKeyEvent event)
    {
        if (event.getKey() == Key.BACK)
        {
            setReceiveInputWhenHolding(false);
            getOwners().get(0).setSneaking(true);
            sneakWalking = true;
            return true;
        }
        return false;
    }
    
    @Override
    public void onKeyPressed(MapKeyEvent event)
    {
        Player player = event.getPlayer();
        
        if (player.getUniqueId().equals(owner)
                || _public && (player.hasPermission("bigdoors.gui.controller.use")
                        || player.hasPermission("bigdoors.gui.controller.adminuse"))
                || !_public && player.hasPermission("bigdoors.gui.controller.adminuse"))
        {
            super.onKeyPressed(event);
            updateSneakWalking(event);
        }
        else if (_public)
        {
            player.sendMessage(ChatColor.RED
                    + "You do not have permission to use BigDoors Map GUI Controllers from other players!");
        }
        else if (!_public)
        {
            player.sendMessage(ChatColor.RED
                    + "You do not have permission to use private BigDoors Map GUI Controllers!");
        }
    }
    
    @Override
    public void onAttached()
    {
        name = properties.get("name", String.class);
        owner = properties.get("owner", UUID.class);
        _public = properties.get("public", false);
        
        long[] doors = properties.get("doors", new long[0]);
        Map<Long, String> doorAliases = new HashMap<>();
        
        for (long door : doors)
        {
            String alias = properties.get("alias." + door, String.class);
            if (alias != null) doorAliases.put(door, alias);
        }
        
        doorList = new MapWidgetListView(
                Arrays.stream(doors).boxed().collect(Collectors.toCollection(ArrayList::new)), doorAliases);
        
        setGlobal(true);
        setUpdateWithoutViewers(true);
        setSessionMode(MapSessionMode.ONLINE);
        setMasterVolume(0.3f);
        reload();
    }
    
    @Override
    public void onLeftClick(final MapClickEvent event)
    {
        Player player = event.getPlayer();
        if (player.isSneaking()) return;
        
        if (player.getUniqueId().equals(owner)
                || _public && (player.hasPermission("bigdoors.gui.controller.use")
                        || player.hasPermission("bigdoors.gui.controller.adminuse"))
                || !_public && player.hasPermission("bigdoors.gui.controller.adminuse"))
        {
            event.setCancelled(true);
            doorList.sendStatusChange("buttonClick", event.getX() + "," + event.getY());
        }
        else if (_public)
        {
            player.sendMessage(ChatColor.RED
                    + "You do not have permission to use BigDoors Map GUI Controllers from other players!");
        }
        else if (!_public)
        {
            player.sendMessage(ChatColor.RED
                    + "You do not have permission to use private BigDoors Map GUI Controllers!");
        }
    }
    
    @Override
    public void onKey(MapKeyEvent event)
    {
        Player player = event.getPlayer();
        
        if (player.getUniqueId().equals(owner)
                || _public && (player.hasPermission("bigdoors.gui.controller.use")
                        || player.hasPermission("bigdoors.gui.controller.adminuse"))
                || !_public && player.hasPermission("bigdoors.gui.controller.adminuse"))
        {
            super.onKey(event);
        }
    }
    
    @Override
    public void onKeyReleased(MapKeyEvent event)
    {
        Player player = event.getPlayer();
        
        if (player.getUniqueId().equals(owner)
                || _public && (player.hasPermission("bigdoors.gui.controller.use")
                        || player.hasPermission("bigdoors.gui.controller.adminuse"))
                || !_public && player.hasPermission("bigdoors.gui.controller.adminuse"))
        {
            super.onKeyReleased(event);
        }
    }
    
    public void reload()
    {
        clearWidgets();
        
        window = new MapWidgetWindow();
        window.setBounds(0, 0, 128, getHeight());
        window.getTitle().setText("BDC  " + name);
        
        addWidget(window);
        
        sneakWalking = getOwners().get(0).isSneaking();
        setReceiveInputWhenHolding(!sneakWalking);
        
        window.addWidget(new MapWidgetButtonColorable()
        {
            @Override
            public void onAttached()
            {
                setText("Open ALL");
                setBackgroundFocusColor(new Color(0x55FF55));
            }
            
            @Override
            public void onFocus()
            {
                display.playSound(SoundEffect.CLICK_WOOD);
            }
            
            @Override
            public void onActivate()
            {
                display.playSound(SoundEffect.fromName("block.note_block.pling"), 1.0f, 1.0f);
                for (long doorID : doorList.getDoorIDs())
                    doorList.sendStatusChange("doorOpen", doorID);
            }
        }).setBounds(3, 15, 60, 15);
        
        window.addWidget(new MapWidgetButtonColorable()
        {
            @Override
            public void onAttached()
            {
                setText("Close ALL");
                setBackgroundFocusColor(new Color(0xFF5555));
            }
            
            @Override
            public void onFocus()
            {
                display.playSound(SoundEffect.CLICK_WOOD);
            }
            
            @Override
            public void onActivate()
            {
                display.playSound(SoundEffect.fromName("block.note_block.pling"), 1.0f, 0.5f);
                for (long doorID : doorList.getDoorIDs())
                    doorList.sendStatusChange("doorClose", doorID);
            }
        }).setBounds(65, 15, 60, 15);
        
        window.addWidget(doorList).setBounds(3, 32, 122, getHeight() - 39);
        
    }
    
    public boolean addDoor(long doorID)
    {
        if (doorList.getDoorIDs().contains(doorID)) return false;
        else
        {
            doorList.getDoorIDs().add(doorID);
            properties.set("doors", doorList.getDoorIDs().stream().mapToLong(l -> l).toArray());
            doorList.invalidate();
            return true;
        }
    }
    
    public boolean hasDoor(long doorID)
    {
        return doorList.getDoorIDs().contains(doorID);
    }
    
    public void setAlias(long doorID, String alias)
    {
        doorList.setAlias(doorID, alias);
        properties.set("alias." + doorID, alias);
        doorList.invalidate();
    }
    
    public boolean removeDoor(long doorID)
    {
        if (!doorList.getDoorIDs().contains(doorID)) return false;
        else
        {
            doorList.getDoorIDs().remove(doorID);
            properties.set("doors", doorList.getDoorIDs().stream().mapToLong(l -> l).toArray());
            doorList.invalidate();
            return true;
        }
    }
    
    public List<String> listDoors()
    {
        List<String> output = new ArrayList<>();
        for (long doorID : doorList.getDoorIDs())
        {
            String alias = properties.get("alias." + doorID, String.class);
            output.add("#" + doorID + (alias == null ? "" : " (" + alias + ")"));
        }
        return output;
    }
    
    public boolean isPublic()
    {
        return _public;
    }
    
    public void setPublic(boolean _public)
    {
        this._public = _public;
        properties.set("public", _public);
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
        properties.set("name", name);
    }
    
    public UUID getOwner()
    {
        return owner;
    }
    
    public void setOwner(UUID owner)
    {
        this.owner = owner;
        properties.set("owner", owner);
    }
}
