# BigDoors Map GUI Controller

This is an add-on for BigDoors that adds map GUIs to control doors!<br />
Created by bbayu for public use

## Getting Started

If you want to help develop this plugin, please clone this repository onto your local IDE, and start from there. <br />
This project uses Maven to handle dependencies. To build the project, do `mvn clean verify`.

## Running the plugin

This Spigot plugin is built against 1.13.2. Any version above 1.13.2 should work with this plugin. <br />
This plugin depends on the following plugins:
- [BigDoors](https://www.spigotmc.org/resources/big-doors.58669/) v0.1.8.32
- [BKCommonLib](https://www.spigotmc.org/resources/bkcommonlib.39590/) v1.17.1-v4

Any version higher than the listed version should still work with this.

### Commands

There is only 1 base command, `/bigdoorsguicontroller`, or `/bdguic` for short.

Sub-commands include:
- `/bdguic get <name>`: Obtains a Map GUI Controller, and assigns it the name provided.
- `/bdguic adddoor <doorID>`: Adds the door specified to the Map GUI Controller.
- `/bdguic remdoor <doorID>`: Removes the door specified from the Map GUI Controller.
- `/bdguic public` or `/bdguic unlock`: Makes the Map GUI Controller public.
- `/bdguic private` or `/bdguic lock`: Makes the Map GUI Controller private.

Except for `/bdguic get`, all other commands require you to hold the Map GUI Controller in your **main hand**.

### Permissions

There are 3 permissions attached with the plugin:
- `bigdoors.gui.controller.create`: Allows the use of `/bdguic`. Default given to OP.
- `bigdoors.gui.controller.use`: Allows the use of **public** Map GUI Controllers. Default given to all users.
- `bigdoors.gui.controller.adminuse`: Allows the use of **private** Map GUI Controllers. Default given to OP.

If you own the Map GUI Controller (you created it), then a permission check is required for you to use your own Map GUI Controller.

### Further information

For further information, please head to the website: <https://bbayu.gitlab.io/bigdoors-map-gui-controller/>

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Cyrus M. X. Li** - *Maintainer* - @bbayu
